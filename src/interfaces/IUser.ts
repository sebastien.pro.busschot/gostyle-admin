import {ICity} from './ICity';
import {IRole} from './IRole';

export interface IUser {
  id: number;
  surname: string;
  lastname: string;
  address: string;
  deleted: boolean;
  username: string;
  city: ICity;
  role: IRole;
}
