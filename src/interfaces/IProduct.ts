import {ICouponProduct} from './ICouponProduct';
import {IProductFile} from './IProductFile';

export interface IProduct {
  productId: number;
  product: string;
  description: string;
  quantity: number;
  unitPrice: number;
  siteUrl: string;
  deleted: boolean;
  couponProduct: ICouponProduct[];
  file: IProductFile;
}
