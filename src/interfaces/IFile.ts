import {IProduct} from './IProduct';

export interface IFile {
  fileId: number;
  fileName: string;
  product: IProduct;
}
