export interface ICity {
  cityId: number;
  zipCode: number;
  city: string;
}
