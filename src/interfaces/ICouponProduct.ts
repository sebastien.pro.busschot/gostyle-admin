import {IProduct} from './IProduct';
import {ICoupon} from './ICoupon';

export interface ICouponProduct {
  product: IProduct;
  coupon: ICoupon;
}
