import {IProduct} from './IProduct';

export interface IProductFile {
  fileId: number;
  fileName: string;
  product: IProduct;
}
