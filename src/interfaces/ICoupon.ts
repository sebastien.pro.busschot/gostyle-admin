export interface ICoupon {
  couponId: number;
  coupon: string;
  issueDate: Date;
  expiryDate: Date;
  percentageDiscount: number;
  couponDescription: string;
  conditions: string;
  barcode: string;
  deleted: boolean;
}
