import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class UriService {
  constructor(private router: Router) {
  }

  lastSegment = '';

  getLastSegment(): string {
    const url = this.router.url.split('/');
    return url[url.length - 1];
  }

  getId(): number {
    if (this.lastSegment !== '') {
      return Number(this.lastSegment);
    } else {
      return Number(this.getLastSegment());
    }
  }
}
