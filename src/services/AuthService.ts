export class AuthService {
  static isAuth(): boolean {
    const haveToken = localStorage.getItem('token') != null;
    const haveValidityTime = localStorage.getItem('tokenValidity') != null;
    let tokenTimeIsValid = false;
    if (haveValidityTime){
      // @ts-ignore
      const validity = new Date(+localStorage.getItem('tokenValidity'));
      const now = new Date(Date.now());
      tokenTimeIsValid = validity > now;
    }

    const ownRules = '' + localStorage.getItem('role');
    const rulesIsPermited = ownRules === 'admin';
    return haveToken && tokenTimeIsValid && rulesIsPermited;
  }
}
