import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ServConfig} from '../app/ServConfig';
import {LoginForm} from '../models/LoginForm';
import {ILogin} from '../interfaces/ILogin';
import {Injectable} from '@angular/core';
import {IFile} from '../interfaces/IFile';

@Injectable(
  {
    providedIn: 'root'
  }
)
export class API {
  constructor(protected http: HttpClient){
  }

  getHeader(): { headers: HttpHeaders } {
    const header = new HttpHeaders()
      .set('Authorization', `Bearer ${localStorage.getItem('token')}`);

    return {headers: header};
  }

  login(loginForm: LoginForm): Observable<ILogin> {
    const body: HttpParams = new HttpParams()
      .set('username', loginForm.username)
      .set('password', loginForm.password)
      .set('grant_type', 'password');

    return this.http.post<ILogin>(ServConfig.getUri('auth/token'), body.toString(), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Authorization', `Basic ${ServConfig.secretClient}`)
    });
  }

  uploadFile(formdata: FormData): Observable<IFile> {
    return this.http.post<IFile>(ServConfig.getUri('files'), formdata, {
      headers: new HttpHeaders()
        .set('Content-Type', 'multipart/form-data')
        .set('Authorization', `Bearer ${localStorage.getItem('token')}`)
    });
  }

  get<T>(route: string): Observable<T> {
    return this.http.get<T>(ServConfig.getUri(route), this.getHeader());
  }

  create<T>(route: string, body: {}): Observable<T> {
    return this.http.post<T>(ServConfig.getUri(route), body, this.getHeader());
  }

  update<T>(route: string, body: {}): Observable<T> {
    return this.http.put<T>(ServConfig.getUri(route), body, this.getHeader());
  }

  delete<T>(route: string): Observable<T> {
    return this.http.delete<T>(ServConfig.getUri(route), this.getHeader());
  }
}
