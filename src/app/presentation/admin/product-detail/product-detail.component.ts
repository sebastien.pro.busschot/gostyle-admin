import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../../services/AuthService';
import {UriService} from '../../../../services/UriService';
import {FormControl, FormGroup} from '@angular/forms';
import {IProduct} from '../../../../interfaces/IProduct';
import {API} from '../../../../services/API';
import {ServConfig} from '../../../ServConfig';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  constructor(private router: Router, private uri: UriService, private api: API) {
    this.productForm = new FormGroup({
      product: new FormControl('', {}),
      site: new FormControl('', {}),
      deleted: new FormControl('', {}),
      stock: new FormControl('', {}),
      price: new FormControl('', {}),
      description: new FormControl('', {}),
    });
  }
  productId = 0;
  productForm: FormGroup;
  product: IProduct = {} as IProduct;

  ngOnInit(): void {
    if (!AuthService.isAuth()) {
      this.router.navigate(['login']);
    }
    this.productId = this.uri.getId();
    if ( this.productId !== 0 ) {
      this.api.get<IProduct>(`products/${this.productId}`).subscribe(
        (response) => {
          this.product = response;
        }
      );
    }
  }

  getPictureUrl(): string {
    return ServConfig.getUri(`files/show/${this.product.file.fileName}`);
  }

  softDelete(): void {
   this.api.delete<IProduct>(`products/${this.productId}`).subscribe(
     (response) => {
       this.product.deleted = true;
     }
   );
  }

  onChange(event: Event): void {
    const formData = new FormData();
    // @ts-ignore
    if ( event.target.files.length > 0 ) {
      // @ts-ignore
      const file = event.target.files[0];
      // @ts-ignore
      formData.append('file', file);
      this.api.uploadFile(formData).subscribe(
        (response) => {
          console.log(response);
        }
      );
    }
  }

  onSubmit(): void {
    const body = {
      product: this.productForm.value.product,
      description: this.productForm.value.description,
      quantity: this.productForm.value.stock,
      unitPrice: this.productForm.value.price,
      siteUrl: this.productForm.value.site,
      file: {
        fileId : this.product.file.fileId
      }
    };
    if (this.uri.getId() === 0) {
      this.api.create<IProduct>('products', body).subscribe(
        (response) => {
          console.log(response);
        }
      );
    } else {
      this.api.update<IProduct>(`products/${this.productId}`, body).subscribe(
        (response) => {
          console.log();
        }
      );
    }
  }
}
