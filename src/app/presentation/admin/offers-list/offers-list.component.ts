import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../../services/AuthService';
import {ICouponProduct} from '../../../../interfaces/ICouponProduct';
import {API} from '../../../../services/API';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-offers-list',
  templateUrl: './offers-list.component.html',
  styleUrls: ['./offers-list.component.css']
})
export class OffersListComponent implements OnInit {

  constructor(private router: Router, private api: API) {
   this.searchForm = new FormGroup({
      select: new FormControl('',{})
    })
  }
  searchForm: FormGroup;
  searchString = '';
  offers: ICouponProduct[] = [];
  offersFiltred: ICouponProduct[] = [];

  ngOnInit(): void {
    if (!AuthService.isAuth()) {
      this.router.navigate(['login']);
    }

    this.api.get<ICouponProduct[]>('couponsProducts').subscribe(
      (response) => {
        this.offers = response;
        this.offersFiltred = response;
        console.log(response);
      }
    );
  }

   onChange($event: Event): void {
      const search = `${$event}`;
      const newValue: ICouponProduct[] = [];
      for (const offer of this.offers) {
        if ( offer.product.product.search(search) !== -1) {
          newValue.push(offer);
        }
      }
      this.offersFiltred = newValue;
   }

}
