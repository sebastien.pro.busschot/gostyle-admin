import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../../services/AuthService';
import {IUser} from '../../../../interfaces/IUser';
import {API} from '../../../../services/API';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  constructor(private router: Router, private api: API) {
    this.searchForm = new FormGroup({
      search: new FormControl('', {})
    });
  }

  users: IUser[] = [];
  usersShowed: IUser[] = [];
  searchForm: FormGroup;

  ngOnInit(): void {
    if (!AuthService.isAuth()) {
      this.router.navigate(['login']);
    }

    this.api.get<IUser[]>('users').subscribe(
      (response) => {
        this.users = response;
        this.usersShowed = response;
      }
    );
  }

  search(event: Event): void {
    const searchString = `${event}`.toLowerCase();
    const toReturn: IUser[] = [];
    for (const user of this.users) {
      let isValid = false;

      isValid = user.surname.toLowerCase().search(searchString.toLowerCase()) !== -1 ? true : isValid;
      isValid = user.lastname.toLowerCase().search(searchString.toLowerCase()) !== -1 ? true : isValid;
      isValid = user.username.toLowerCase().search(searchString.toLowerCase()) !== -1 ? true : isValid;
      isValid = user.address.toLowerCase().search(searchString.toLowerCase()) !== -1 ? true : isValid;
      isValid = user.city.city.toLowerCase().search(searchString.toLowerCase()) !== -1 ? true : isValid;
      isValid = `${user.city.zipCode}`.toLowerCase().search(searchString.toLowerCase()) !== -1 ? true : isValid;
      isValid = user.role.role.toLowerCase().search(searchString.toLowerCase()) !== -1 ? true : isValid;

      if (isValid) {
        toReturn.push(user);
      }
    }
    this.usersShowed = toReturn;
  }

}
