import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../services/AuthService';
import {Router} from '@angular/router';

@Component({
  selector: 'app-connected-home',
  templateUrl: './connected-home.component.html',
  styleUrls: ['./connected-home.component.css']
})
export class ConnectedHomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (!AuthService.isAuth()) {
      this.router.navigate(['login']);
    }
  }

}
