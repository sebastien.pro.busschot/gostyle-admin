import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../../services/AuthService';
import {IUser} from '../../../../interfaces/IUser';
import {API} from '../../../../services/API';
import {UriService} from '../../../../services/UriService';
import {FormControl, FormGroup} from '@angular/forms';
import {IRole} from '../../../../interfaces/IRole';
import {ICity} from '../../../../interfaces/ICity';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  constructor(private router: Router, private api: API, private uri: UriService) {
    this.userForm = new FormGroup({
      surname: new FormControl('', {}),
      password: new FormControl('', {}),
      lastname: new FormControl('', {}),
      username: new FormControl('', {}),
      address: new FormControl('', {}),
      city: new FormControl('', {}),
      zipcode: new FormControl('', {}),
      roleId: new FormControl('', {})
    });
  }

  userId = -1;
  user: IUser = {
    address: '',
    city: {} as ICity,
    deleted: false,
    id: 0,
    lastname: '',
    role: {} as IRole,
    surname: '',
    username: ''
  };
  userForm: FormGroup;
  roles: IRole[] = [];

  ngOnInit(): void {
    if (!AuthService.isAuth()) {
      this.router.navigate(['login']);
    }

    this.userId = this.uri.getId();

    if (this.userId > 0) {
      this.api.get<IUser>(`users/${this.userId}`).subscribe(
        (response) => {
          this.user = response;
        }
      );
    }

    this.api.get<IRole[]>('roles').subscribe(
      (response) => {
        this.roles = response;
      }
    );
  }

  onSubmit(): void {
    const body = {
      username : this.userForm.value.username,
      password : this.userForm.value.password,
      surname : this.userForm.value.surname,
      lastname : this.userForm.value.lastname,
      address : this.userForm.value.address,
      city : {
        city: this.userForm.value.city,
        zipCode: this.userForm.value.zipcode
      },
      role : {
        roleId : this.userForm.value.roleId === '' ? this.user.role.roleId : this.getIdRoleByName(this.userForm.value.roleId)
      }
    };

    if (this.userId === 0) {
      this.api.create<IUser>('register', body).subscribe(
        (response) => {
          this.router.navigate([`/admin/user/${response.id}`]);
        }
      );
    } else {
      this.api.update<IUser>(`users/${this.userId}`, body).subscribe(
        (response) => {
          this.router.navigate([`/admin/users`]);
        }
      );
    }
  }

  onDelete(): void {
    this.api.delete<IUser>(`users/${this.userId}`).subscribe(
      (response) => {
        this.user.deleted = true;
      }
    );
  }

  getIdRoleByName(name: string): number {
    for (const role of this.roles) {
      if (role.role === name) {
        return  role.roleId;
      }
    }
    return 0;
  }

}
