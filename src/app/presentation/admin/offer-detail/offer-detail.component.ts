import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../../services/AuthService';
import {UriService} from '../../../../services/UriService';
import {ServConfig} from '../../../ServConfig';
import {API} from '../../../../services/API';
import {FormControl, FormGroup} from '@angular/forms';
import {ICouponProduct} from '../../../../interfaces/ICouponProduct';
import {ICoupon} from '../../../../interfaces/ICoupon';
import {IProduct} from "../../../../interfaces/IProduct";


@Component({
  selector: 'app-offer-detail',
  templateUrl: './offer-detail.component.html',
  styleUrls: ['./offer-detail.component.css']
})
export class OfferDetailComponent implements OnInit {

  constructor(private router: Router, private api: API, private uri: UriService) {
  }

  ngOnInit(): void {
    if (!AuthService.isAuth()) {
      this.router.navigate(['login']);
    }
  }
}
