import { Component, OnInit } from '@angular/core';
import {LoginForm} from '../../../models/LoginForm';
import {API} from '../../../services/API';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/AuthService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private api: API
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  ngOnInit(): void {
    if (AuthService.isAuth()) {
      this.router.navigate(['admin']);
    }
  }

  onSubmintForm(): void {
    const loginForm: LoginForm = new LoginForm(this.loginForm.value.email, this.loginForm.value.password);
    const login = this.api.login(loginForm);
    login.subscribe((response) => {
      localStorage.setItem('token', response.access_token);

      const date = new Date(Date.now() + ( response.expires_in * 999 ));
      localStorage.setItem('tokenValidity', `${date.getTime()}`);
      localStorage.setItem('role', 'admin');

      if (AuthService.isAuth()) {
        this.router.navigate(['admin']);
      }
    });
  }

}
