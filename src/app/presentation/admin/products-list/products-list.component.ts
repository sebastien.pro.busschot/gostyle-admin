import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../../services/AuthService';
import {FormControl, FormGroup} from '@angular/forms';
import {API} from '../../../../services/API';
import {IProduct} from '../../../../interfaces/IProduct';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  constructor(private router: Router, private api: API) {
    this.searchForm = new FormGroup({
      select: new FormControl('', {})
    });
  }
  searchForm: FormGroup;
  searchString = '';
  products: IProduct[] = [];
  productsFiltred: IProduct[] = [];

  ngOnInit(): void {
    if (!AuthService.isAuth()) {
      this.router.navigate(['login']);
    }

    this.api.get<IProduct[]>('products').subscribe(
      (response) => {
        this.products = response;
        this.productsFiltred = response;
      }
    );

  }

  onChange($event: Event): void {
    const search = `${$event}`;
    const newValue: IProduct[] = [];
    for (const product of this.products) {
      if ( product.product.search(search) !== -1) {
        newValue.push(product);
      }
    }
    this.productsFiltred = newValue;
  }

}
