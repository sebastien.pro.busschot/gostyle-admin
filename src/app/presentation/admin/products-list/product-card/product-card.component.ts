import {Component, Input, OnInit} from '@angular/core';
import {IProduct} from '../../../../../interfaces/IProduct';
import {ServConfig} from '../../../../ServConfig';
import {API} from '../../../../../services/API';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {

  constructor(private api: API) { }

  @Input() product: IProduct = {} as IProduct;
  pictureUrl = '';

  ngOnInit(): void {
    this.pictureUrl = ServConfig.getUri(`files/show/${this.product.file.fileName}`);
    this.api.get(`files/show/${this.product.file.fileName}`).subscribe(
      (response) => {
        console.log(response);
      }
    );
  }

}
