import { Component, OnInit } from '@angular/core';
import {Products} from '../../../models/Products';
import {Coupons} from '../../../models/Coupons';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

  product: Products;
  coupons: Array<Coupons>;

  constructor() {
    const urlImage = 'https://s1.qwant.com/thumbr/0x380/3/8/670764f439871f59f0ab005aac8d211095ed54fc864260a09f887f0146bc2d/15362-chaussures-ellesse-taggia-noire-femme-vue-exterieure.png?u=https%3A%2F%2Fmedia.chausport.com%2Fmedia%2Fcatalog%2Fproduct%2Fcache%2F1%2Fimage%2F9df78eab33525d08d6e5fb8d27136e95%2F1%2F5%2F15362-chaussures-ellesse-taggia-noire-femme-vue-exterieure.png&q=0&b=1&p=0&a=1';
    const description = 'Le Lorem Ipsum est simplement du faux texte employé dans la composition\n' +
      ' et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard \n' +
      'de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla\n' +
      ' ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.\n' +
      ' Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique\n' +
      ' informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans\n' +
      ' les années 1960 grâce à la vente de feuilles Letraset contenant des passages\n' +
      ' du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications \n' +
      'de mise en page de texte, comme Aldus PageMaker.';
    this.product = new Products(1, 'Sausure', description, 10, 9.99, urlImage, false);
    this.coupons = [new Coupons(1, 'Remise pourrie', new Date(Date.now()), new Date(Date.now()), 0, 'Truc pas cher avec remise', 'Acheter', '45645qzd5644', false)];
  }

  ngOnInit(): void {
  }

}
