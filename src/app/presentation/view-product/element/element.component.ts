import {Component, Input, OnInit} from '@angular/core';
import {Coupons} from '../../../../models/Coupons';

@Component({
  selector: 'app-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.css']
})
export class ElementComponent implements OnInit {
  constructor() {
    this.coupon = new Coupons(0, '', new Date(Date.now()), new Date(Date.now()), 0, '', '', '', false);
  }

  @Input() coupon: Coupons;

  ngOnInit(): void {
  }

}
