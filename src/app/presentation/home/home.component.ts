import { Component, OnInit } from '@angular/core';
import {Buttons} from '../../../models/Buttons';
import {HomeCard} from './home-card/models/HomeCard';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() {
    this.buttons = [
      new Buttons('', 'Completes'),
      new Buttons('', 'Skate Schuhe'),
      new Buttons('', 'Skate Decks')
    ];
    this.background = 'http://www.theriderpost.com/wp-content/uploads/2017/04/skate.jpg';
    this.homeCards = [
      new HomeCard('', 'New skate stuff', [
        new Buttons('demo', 'Street Stuf'),
        new Buttons('demo', 'Piñatas')
      ]),
      new HomeCard('', 'Life style', [
        new Buttons('demo', 'Accesoires'),
        new Buttons('demo', 'Shoes')
      ]),
      new HomeCard('', 'Skates', [
        new Buttons('demo', 'Creations'),
        new Buttons('demo', 'Weels')
      ]),
      new HomeCard('', 'Events', [
        new Buttons('demo', 'Contest'),
        new Buttons('demo', 'Concerts')
      ]),
    ];
  }
  buttons: Buttons[];
  background: string;
  homeCards: HomeCard[];

  ngOnInit(): void {
  }

}
