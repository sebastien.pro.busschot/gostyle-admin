import {Component, Input, OnInit} from '@angular/core';
import {HomeCard} from './models/HomeCard';

@Component({
  selector: 'app-home-card',
  templateUrl: './home-card.component.html',
  styleUrls: ['./home-card.component.css']
})
export class HomeCardComponent implements OnInit {

  constructor() {
    this.datas = new HomeCard('', '', []);
  }

  @Input() datas: HomeCard;

  ngOnInit(): void {
  }

}
