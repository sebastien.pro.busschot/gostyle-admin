import {Buttons} from '../../../../../models/Buttons';

export class HomeCard {
  constructor(backbround: string, title: string, buttons: Buttons[]) {
    this.background = backbround;
    this.title = title;
    this.buttons = buttons;
  }
  background: string;
  title: string;
  buttons: Buttons[];
}
