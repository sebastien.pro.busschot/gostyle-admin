import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-button',
  templateUrl: './home-button.component.html',
  styleUrls: ['./home-button.component.css']
})
export class HomeButtonComponent implements OnInit {

  constructor() {
    this.url = '';
    this.text = '';
  }
  @Input() url: string;
  @Input() text: string;

  ngOnInit(): void {
    console.log('ok');
  }

}
