import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import {MatButtonModule} from '@angular/material/button';
import { ViewProductComponent } from './presentation/view-product/view-product.component';
import { HomeComponent } from './presentation/home/home.component';
import { LoginComponent } from './presentation/admin/login.component';
import { ElementComponent } from './presentation/view-product/element/element.component';
import { HomeCardComponent } from './presentation/home/home-card/home-card.component';
import { HomeButtonComponent } from './presentation/home/home-button/home-button.component';
import { ConnectedHomeComponent } from './presentation/admin/connected-home/connected-home.component';
import { ProductsListComponent } from './presentation/admin/products-list/products-list.component';
import { ProductDetailComponent } from './presentation/admin/product-detail/product-detail.component';
import { OffersListComponent } from './presentation/admin/offers-list/offers-list.component';
import { OfferDetailComponent } from './presentation/admin/offer-detail/offer-detail.component';
import { UsersListComponent } from './presentation/admin/users-list/users-list.component';
import { UserDetailComponent } from './presentation/admin/user-detail/user-detail.component';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import { MenuComponent } from './presentation/admin/menu/menu.component';
import { LogoutComponent } from './presentation/admin/logout/logout.component';
import { ProductCardComponent } from './presentation/admin/products-list/product-card/product-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    ViewProductComponent,
    HomeComponent,
    LoginComponent,
    ElementComponent,
    HomeCardComponent,
    HomeButtonComponent,
    ConnectedHomeComponent,
    ProductsListComponent,
    ProductDetailComponent,
    OffersListComponent,
    OfferDetailComponent,
    UsersListComponent,
    UserDetailComponent,
    MenuComponent,
    LogoutComponent,
    ProductCardComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
