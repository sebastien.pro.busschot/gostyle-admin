import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {HomeComponent} from './presentation/home/home.component';
import {ViewProductComponent} from './presentation/view-product/view-product.component';
import {LoginComponent} from './presentation/admin/login.component';
import {ConnectedHomeComponent} from './presentation/admin/connected-home/connected-home.component';
import {ProductsListComponent} from './presentation/admin/products-list/products-list.component';
import {ProductDetailComponent} from './presentation/admin/product-detail/product-detail.component';
import {OffersListComponent} from './presentation/admin/offers-list/offers-list.component';
import {OfferDetailComponent} from './presentation/admin/offer-detail/offer-detail.component';
import {UsersListComponent} from './presentation/admin/users-list/users-list.component';
import {UserDetailComponent} from './presentation/admin/user-detail/user-detail.component';
import {LogoutComponent} from './presentation/admin/logout/logout.component';

const routes: Routes = [
  { path : '', component: HomeComponent },
  { path : 'product/:id', component: ViewProductComponent },
  { path : 'login', component: LoginComponent },
  { path : 'logout', component: LogoutComponent},
  { path: 'demo', component: PageNotFoundComponent },
  { path : 'admin', children: [
      { path : '', component: ConnectedHomeComponent},
      { path : 'products', component: ProductsListComponent},
      { path : 'product/:id', component: ProductDetailComponent},
      { path : 'offers', component: OffersListComponent},
      { path : 'offer/:id', component: OfferDetailComponent},
      { path : 'users', component: UsersListComponent},
      { path : 'user/:id', component: UserDetailComponent},
    ]},
  { path: '**', redirectTo: 'demo'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
