import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root',
  }
)
export class ServConfig {
  static APIMethod = 'http';
  static APIUri = '78.222.227.165';
  static APIPort = 8888;
  static secretClient = 'cG9zdG1hbjo=';
  static getUri(route: string): string {
    return `${ServConfig.APIMethod}://${ServConfig.APIUri}:${ServConfig.APIPort}/${route}`;
  }
}
