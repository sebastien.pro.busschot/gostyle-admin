import {Coupons} from './Coupons';

export class QrCodes {
  // tslint:disable-next-line:variable-name
  private _idQrCOde: number;
  // tslint:disable-next-line:variable-name
  private _content: string;
  // tslint:disable-next-line:variable-name
  private _coupon: Coupons;

  constructor(idQrCOde: number, content: string, coupon: Coupons) {
    this._idQrCOde = idQrCOde;
    this._content = content;
    this._coupon = coupon;
  }

  get idQrCOde(): number {
    return this._idQrCOde;
  }

  set idQrCOde(value: number) {
    this._idQrCOde = value;
  }

  get content(): string {
    return this._content;
  }

  set content(value: string) {
    this._content = value;
  }

  get coupon(): Coupons {
    return this._coupon;
  }

  set coupon(value: Coupons) {
    this._coupon = value;
  }
}
