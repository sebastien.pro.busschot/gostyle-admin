export class Coupons {
  // tslint:disable-next-line:variable-name
  private _Coupon_id: number;
  // tslint:disable-next-line:variable-name
  private _coupon: string;
  // tslint:disable-next-line:variable-name
  private _issueDate: Date;
  // tslint:disable-next-line:variable-name
  private _expiryDate: Date;
  // tslint:disable-next-line:variable-name
  private _persentageDiscount: number;
  // tslint:disable-next-line:variable-name
  private _couponDescription: string;
  // tslint:disable-next-line:variable-name
  private _conditions: string;
  // tslint:disable-next-line:variable-name
  private _barcode: string;
  // tslint:disable-next-line:variable-name
  private _deleted: boolean;


  // tslint:disable-next-line:variable-name
  constructor(Coupon_id: number,
              coupon: string,
              issueDate: Date,
              expiryDate: Date,
              persentageDiscount: number,
              couponDescription: string,
              conditions: string,
              barcode: string,
              deleted: boolean
  ) {
    this._Coupon_id = Coupon_id;
    this._coupon = coupon;
    this._issueDate = issueDate;
    this._expiryDate = expiryDate;
    this._persentageDiscount = persentageDiscount;
    this._couponDescription = couponDescription;
    this._conditions = conditions;
    this._barcode = barcode;
    this._deleted = deleted;
  }

  get Coupon_id(): number {
    return this._Coupon_id;
  }

  set Coupon_id(value: number) {
    this._Coupon_id = value;
  }

  get coupon(): string {
    return this._coupon;
  }

  set coupon(value: string) {
    this._coupon = value;
  }

  get issueDate(): Date {
    return this._issueDate;
  }

  set issueDate(value: Date) {
    this._issueDate = value;
  }

  get expiryDate(): Date {
    return this._expiryDate;
  }

  set expiryDate(value: Date) {
    this._expiryDate = value;
  }

  get persentageDiscount(): number {
    return this._persentageDiscount;
  }

  set persentageDiscount(value: number) {
    this._persentageDiscount = value;
  }

  get couponDescription(): string {
    return this._couponDescription;
  }

  set couponDescription(value: string) {
    this._couponDescription = value;
  }

  get conditions(): string {
    return this._conditions;
  }

  set conditions(value: string) {
    this._conditions = value;
  }

  get barcode(): string {
    return this._barcode;
  }

  set barcode(value: string) {
    this._barcode = value;
  }

  get deleted(): boolean {
    return this._deleted;
  }

  set deleted(value: boolean) {
    this._deleted = value;
  }
}
