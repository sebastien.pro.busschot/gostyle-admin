export class Products {
  // tslint:disable-next-line:variable-name
  private _productId: number;
  // tslint:disable-next-line:variable-name
  private _product: string;
  // tslint:disable-next-line:variable-name
  private _description: string;
  // tslint:disable-next-line:variable-name
  private _quantity: number;
  // tslint:disable-next-line:variable-name
  private _unitPrice: number;
  // tslint:disable-next-line:variable-name
  private _imageURL: string;
  // tslint:disable-next-line:variable-name
  private _deleted: boolean;


  constructor(productId: number,
              product: string,
              description: string,
              quantity: number,
              unitPrice: number,
              imageURL: string,
              deleted: boolean) {
    this._productId = productId;
    this._product = product;
    this._description = description;
    this._quantity = quantity;
    this._unitPrice = unitPrice;
    this._imageURL = imageURL;
    this._deleted = deleted;
  }

  get productId(): number {
    return this._productId;
  }

  set productId(value: number) {
    this._productId = value;
  }

  get product(): string {
    return this._product;
  }

  set product(value: string) {
    this._product = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get quantity(): number {
    return this._quantity;
  }

  set quantity(value: number) {
    this._quantity = value;
  }

  get unitPrice(): number {
    return this._unitPrice;
  }

  set unitPrice(value: number) {
    this._unitPrice = value;
  }

  get imageURL(): string {
    return this._imageURL;
  }

  set imageURL(value: string) {
    this._imageURL = value;
  }

  get deleted(): boolean {
    return this._deleted;
  }

  set deleted(value: boolean) {
    this._deleted = value;
  }
}
